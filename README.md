# Logging Aggregator

We use fluentd logging driver of Docker. In this architecture, log forwarders collect log entries from stdout of Docker containers and forward them to log aggregators. Log aggregators transform log data and send them to Elasticsearch for storage and they are visualized by Kibana.

![Architecture Overview](./wiki/architecture.png)

# Forwarder

We use Fluent bit for collecting and forwarding log entries. Let's look at folder **forwarder**, it contains 1 Dockerfile and 1 config file for Fluent bit.

**Dockerfile** is very simple, it bases on fluentbit image and copy fluent-bit.conf to config folder

    FROM fluent/fluent-bit:1.8
    ADD fluent-bit.conf /fluent-bit/etc/

In **fluent-bit.conf**, everything is straighforward. We use [INPUT] forward to listen to log data from port 24224 that is default port of Docker logging driver **fluentd**.

    [INPUT]
    Name forward
    Listen 0.0.0.0
    Port 24224

We use [OUTPUT] forward to send them to port 25225 of aggregator. 
    
    [OUTPUT]
    Name    forward
    Match   *
    Host    log_aggregator
    Port    25225

Log Level is defined in [SERVICE]

    SERVICE]
    log_level info

# Aggregation

We use Fluentd for log aggregation and sending them to log storage, in this case Elasticsearch. Similarly to forwarder, in folder **aggregator**, we have a Dockerfile and Fluentd config files.

**Dockerfile** is also simple, it bases on Fluentd image, install Fluentd's Elasticsearch plugin and copy config files. It exposes to port 25225 because the default port 24224 is already being used by Fluent bit as the forwarder.

    FROM fluent/fluentd:v1.13-1
    
    USER root
    RUN gem install fluent-plugin-elasticsearch
    USER fluent
    WORKDIR /home/fluent
    ENV PATH /home/fluent/.gem/ruby/2.2.0/bin:$PATH
    
    COPY fluent.conf /fluentd/etc
    COPY config.d /fluentd/etc/config.d
    
    EXPOSE 25225
    
    CMD fluentd -c /fluentd/etc/$FLUENTD_CONF -p /fluentd/plugins $FLUENTD_OPT

This is a central logging aggregator, it handles log data come from many applications, so we organize config for each application in a separate config file in folder **config.d**. The main config file **fluent.conf** just defines the source of log data and import all config files in folder **config.d**.

    <source>
      type forward
      bind 0.0.0.0
      port 25225
    </source>
    
    @include config.d/*.conf

**config.d/msv.conf**: An example of config file for MSV_Service app

    <filter msv.**>
      @type parser
      key_name log
      reserve_data true
      <parse>
        @type json
      </parse>
    </filter>

    <match msv.**>
      @type copy
      <store>
        @type elasticsearch
        hosts "#{ENV['ELASTICSEARCH_HOSTS']}"
        port "#{ENV['ELASTICSEARCH_PORT']}"
        user "#{ENV['ELASTICSEARCH_USERNAME']}"
        password "#{ENV['ELASTICSEARCH_PASSWORD']}"
        logstash_format true
        logstash_prefix ${tag}
        logstash_dateformat %Y-%m   
        include_tag_key true
        type_name app_log
        tag_key @log_name
        flush_interval 1s
        reconnect_on_error true
        reload_on_failure true
       reload_connections false
      </store>
      <store>
        @type stdout
      </store>
    </match>

It handles all log data that has tag of pattern **msv.\*\***, then send them to Elasticsearch. There are some setting inside tag **\<store\>** type elasticsearch. For better reference, please read documentation at [Fluentd's elasticsearch plugin](https://docs.fluentd.org/output/elasticsearch).

# Storage & Visualizer

We use Elasticsearch for log storage and Kibana for log data visualization. Refer to [Elastic Stack](https://www.elastic.co/elastic-stack/) for better understanding.

# Deployment

Normally, Elasticsearch & Kibana should deployed as clusters. So we have a **docker-compose.yml** file to deploy Log forwarder & aggregator in Docker container environment.

For demo in developmnent environment, we provide **docker-compose-elastic.yml** to deploy this Logging Aggregator stack in Docker environement. 
Assume in Docker Desktop, simply just run this command:

    docker-compose -f docker-compose-elastic.yml up

will starts the stack running as below

![Logging Aggregator Stack](./wiki/logging-aggregator-stack-docker.png)

# Client App

In **docker-compose.yml**, just set logging driver to "fluentd" like this

    logging:
      driver: "fluentd"
      options:
        fluentd-async-connect: "true"
        tag: msv.Development.service

Logging data of the app will be handled by Logging Aggreagtor stack. We can visualize log data using Kibana as below:

![Kibana Visualizer](./wiki/kibana-discover-gui.png)